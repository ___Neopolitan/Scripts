# [Download Shutdown Scheduler](https://gitlab.com/___Neopolitan/Scripts/-/raw/main/Windows/PowerShell/Shutdown-Scheduler/bin/Shutdown-Scheduler.bat?inline=false)

### Changelog
- v1.0.0
    - Initial Release

- v1.0.1
    - Cleaned up some redundant lines

- v1.0.2
    - Changed location of shutdown cancellation so that textbox times are accurate

- v1.0.3
    - Increased max shutdown delay from 5255940 minutes to 5255999 minutes

- v1.0.4
    - Restructured repository and gave a download link for the one click batch file which you can find above.
    - Added a version number to the main window title

- v1.0.5
    - Fixed broken download link

- v1.0.6
    - Forgot the version bump the script itself
